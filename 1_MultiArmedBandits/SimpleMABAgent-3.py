#Phase 3
### Examine a policy - exploration vs exploitation
### eps-greedy
### vary the steps...what do you notice?

import random
import numpy as np

class Environment:
    def __init__(self):
        self.steps_left = 10000
        self.observations = [0.0, 0.0, 0.0]
        self.pulls = [0, 0, 0]
        
    def get_observation(self):
        return self.observations

    def get_pulls(self):
        return self.pulls

    def set_observation(self, action, reward):
        # Incrementally computed reward averages
        self.observations[action] = ((self.observations[action] * self.pulls[action]-1) + reward) / self.pulls[action]
        
    def set_pulls(self, action):
        self.pulls[action] += 1

    def get_actions(self):
        return [0, 1, 2]

    def is_done(self):
        return self.steps_left == 0

    def action(self, action):
        if self.is_done():
            raise Exception("Game is over")
        self.steps_left -= 1        
        if(action == 0):
            return random.choice([2,4,6])
        elif(action == 1):
            return random.choice([4,5,7])
        else:
            return random.choice([1,9,11])
            

class Agent:
    def __init__(self):
        self.total_reward = 0.0
        ##Epsilon greedy policy
        self.eps = 0.15

    def step(self, env):
        current_obs = env.get_observation()
        actions = env.get_actions()
        ##Implement the eps greedy policy
        rnd = random.random()
        if(rnd > self.eps):
            #Choose action with largest reward observed to date            
            armSelected = np.argmax(env.get_observation())
        else:
            #Choose random exploratory action
            armSelected = random.choice(actions)
            
        reward = env.action(armSelected)

        # Update observations
        env.set_pulls(armSelected)
        env.set_observation(armSelected, reward)
        self.total_reward += reward

if __name__ == "__main__":
    env = Environment()
    agent = Agent()

    while not env.is_done():
        agent.step(env)

    print("Action with max avg reward", np.argmax(env.get_observation()))
    print("Pulls", *env.get_pulls())
    print("Observations", *env.get_observation()) 
    print("Total reward got: %.4f" % agent.total_reward)
