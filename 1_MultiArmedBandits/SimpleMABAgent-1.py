# Phase 1
### Extend the bandit tasks to 3 bandits
### Rewards are as follows (bandit 1 randInt[2, 4, 6], bandit 2 randInt[4, 5, 7], bandit 3 randInt[1, 9, 11])
### Actions will be from 1-3
### Give the agent 100 episodes/steps 
import random


class Environment:
    def __init__(self):
        self.steps_left = 100

    def get_observation(self):
        return [0.0, 0.0, 0.0]

    def get_actions(self):
        return [0, 1, 2]

    def is_done(self):
        return self.steps_left == 0

    def action(self, action):
        if self.is_done():
            raise Exception("Game is over")
        self.steps_left -= 1        
        if(action == 0):
            return random.choice([2,4,6])
        elif(action == 1):
            return random.choice([4,5,7])
        else:
            return random.choice([1,9,11])
            

class Agent:
    def __init__(self):
        self.total_reward = 0.0

    def step(self, env):
        current_obs = env.get_observation()
        actions = env.get_actions()
        reward = env.action(random.choice(actions))
        self.total_reward += reward


if __name__ == "__main__":
    env = Environment()
    agent = Agent()

    while not env.is_done():
        agent.step(env)
    
    print("Total reward got: %.4f" % agent.total_reward)
