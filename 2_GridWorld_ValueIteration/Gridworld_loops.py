import numpy as np

# global variables
BOARD_ROWS = 3
BOARD_COLS = 4
WIN_STATE = (0, 3)
START = (2, 0)

class State:
    def __init__(self, state=START):        
        self.state = state
        self.isEnd = False        

    def getReward(self, state):
        if state == WIN_STATE:
            return 1        
        else:
            return 0

    def isEndFunc(self):
        if (self.state == WIN_STATE):
            self.isEnd = True

    def nxtPosition(self, action):        
        if action == "up":                
            nxtState = (self.state[0] - 1, self.state[1])                
        elif action == "down":
            nxtState = (self.state[0] + 1, self.state[1])
        elif action == "left":
            nxtState = (self.state[0], self.state[1] - 1)
        else:
            nxtState = (self.state[0], self.state[1] + 1)
            
        if (nxtState[0] >= 0) and (nxtState[0] <= 2):
            if (nxtState[1] >= 0) and (nxtState[1] <= 3):                    
                    return nxtState # if next state legal
        return self.state # Any move off the grid leaves state unchanged

# Agent 

class Agent:

    def __init__(self):
        self.states = []
        self.actions = ["up", "down", "left", "right"]
        self.State = State()
        self.discount = 0.9        
        
        # initialise state values
        self.state_values = {}        
        for i in range(BOARD_ROWS):
            for j in range(BOARD_COLS):
                self.state_values[(i, j)] = 0  # set initial value to 0

        self.new_state_values = {}
            
    def value_iteration(self, episodes):
        # Value iteration implementation                
        x = 0
        # Loop through all the states rows * cols        
        while x < episodes:
            i = 0
            j = 0
            while i < BOARD_ROWS:            
                while j < BOARD_COLS:                                                    
                    j += 1                    
                i += 1
                j = 0                            
            x += 1            
            
    def showValues(self):
        for i in range(0, BOARD_ROWS):
            print('----------------------------------')
            out = '| '
            for j in range(0, BOARD_COLS):
                out += str(self.state_values[(i, j)]).ljust(6) + ' | '
            print(out)
        print('----------------------------------')


if __name__ == "__main__":
    ag = Agent()
    ag.value_iteration(100)
    print(ag.showValues())
