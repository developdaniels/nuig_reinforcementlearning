import numpy as np
import random as random
import matplotlib.pyplot as plt

#GLOBAL VARIABLES
NUM_ROWS = 5
NUM_COLUMNS = 5
START = (0, 0)
GOAL = (4, 4)
HOLES = [(1,0),(3,1),(4,2),(1,3)]

class State:
    def __init__(self, state):        
        self.coordinates = state

    def isHole(self):
        #Retrieve if the state is a hole
        for hole in HOLES:
            if hole == ((self.coordinates[0],self.coordinates[1])):
                return True
        return False

    def getReward(self):
        #Retrieve the reward for a given state.
        #Goal = 10
        #Hole = -5
        #Others = -1
        if self.coordinates == GOAL:
            return 10
        elif self.isHole():
            return -5
        else:
        #Border is the same -1?
            return -1

    def isTerminalState(self):
        #Goal and holes are terminal states.
        if (self.coordinates == GOAL or self.isHole()):
            return True
        else:
            return False

    def isStateInsideGridBoundaries(self, state):
        #Returns if the state is inside the grid or not.
        if (state[0] >= 0) and (state[0] < NUM_ROWS) and (state[1] >= 0) and (state[1] < NUM_COLUMNS):  
            return True
        else:
            return False

    def getNextState(self, action):     
        #Retrieve a new state, given by the current state + action taken
        #This is deterministic, so it will always move   
        if action == 'U': #UP
            nextState = (self.coordinates[0] - 1, self.coordinates[1])                
        elif action == 'D': #DOWN
            nextState = (self.coordinates[0] + 1, self.coordinates[1])
        elif action == 'L': #LEFT
            nextState = (self.coordinates[0], self.coordinates[1] - 1)
        elif action == 'R': #RIGHT
            nextState = (self.coordinates[0], self.coordinates[1] + 1)
        else:
            raise(str.format('Error: action not expected. {}',action))

        #If next state is inside the grid, return it. Otherwise return the current state.            
        if self.isStateInsideGridBoundaries(nextState):                  
            return State(nextState)
        else: 
            return State(self.coordinates) 

class Agent:

    def __init__(self):
        self.actions = ['U', 'D', 'L', 'R'] #UP, DOWN, LEFT, RIGHT

        self.alpha = 0.5    #Discount factor for the future reward in the Bellman Equation
        self.gamma = 0.9    #Weight for next step QValue in the Bellman Equation
        self.episilon = 0.1 #Percentage of random choices rather than following the policy
        
        #All states and actions start with QValue of 0
        self.state_values = {}        
        for i in range(NUM_ROWS):
            for j in range(NUM_COLUMNS):
                for action in self.actions:
                    self.state_values[(i, j),action] = 0
                    
        self.rewardByEpisodes = {}

    def useRandomExplorativeAction(self):
        #If random value is higher than epislon, take the greed stragy. Otherwise, take the random
        if (random.random() > self.episilon):
            return False
        else:
            return True
    
    def getBestActionByState(self, state):
        #Given a state, returns the action which has the max QValue
        maxQValue = self.getMaxQValueByState(state)
        
        #Matches the action with the max value by state
        for action in self.actions:
            if (self.state_values[state.coordinates,action] == maxQValue):
                return action
        
        raise('Error occurred. Maybe a new action is implemented?')
            
    def getMaxQValueByState(self,state):
        #Given a state, returns the best possible QValue for it. The value of the best action
        qValuesByState = []
        for action in self.actions:
            qValuesByState.append(self.state_values[state.coordinates,action])
        return max(qValuesByState)
    
    def getQValueByStateAndAction(self,state,action):
        return self.state_values[state.coordinates,action]
    
    def getActioneGreedStrategy(self,state):
        
        if (self.useRandomExplorativeAction()):
            #Get random explorative action
            return random.choice(self.actions)
        else:
            #Get the action with largest reward observed to date
            return self.getBestActionByState(state)
        
    def calculateBellmanEquation(self, qValueForCurrentState, currentActionReward, maxQValueNextState):
        #Q(s,a) = Q(s,a) +              alpha[        Reward(s,a) +          gamma      * maxQ(s',a')           - Q(s,a)  ] 
        return qValueForCurrentState + (self.alpha * (currentActionReward + (self.gamma * maxQValueNextState) - qValueForCurrentState))
            
    def run(self, episodes):
        
        for episode in range(episodes):
            self.rewardByEpisodes[episode] = 0
            
            #Every episode starts on the START cell of the grid
            currentState = State(START)
            #While the current state is not a terminal state
            while (currentState.isTerminalState() == False):
                
                #Get the action. Most of the times is the best action known so far, a minor part (episilon %) a random action
                action = self.getActioneGreedStrategy(currentState)
                
                #Go to the selected state. Can endup on the same if tries to go outside the grid boundaries
                nextState = currentState.getNextState(action)

                #Get the current QValue of the current state + action taken:    Q(s,a) 
                qValueForCurrentState = self.getQValueByStateAndAction(currentState,action)
                
                #Reward of the next state:  (Reward(s,a))
                currentActionReward = nextState.getReward()

                #Get the max Q Value possible for the next state:   (maxQ(s',a'))    
                maxQValueNextState = self.getMaxQValueByState(nextState)
                
                #Update the QValue for the current state + action using the bellman equation
                self.state_values[currentState.coordinates,action] = self.calculateBellmanEquation(qValueForCurrentState, currentActionReward, maxQValueNextState)
                
                #Accumulates the reward for this step
                self.rewardByEpisodes[episode] += currentActionReward
                
                #Updates the current state
                currentState = nextState
                
            
    def showValues(self, showAll = True):
        for i in range(0, 5):
            if (showAll):
                print('------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------')
            else:
                print('------------------------------------------------------------------')
                
            out = '| '
            for j in range(0, 5):
                # for action in self.actions:

                valueUp = round(self.state_values[(i, j),'U'],2)
                valueDown = round(self.state_values[(i, j),'D'],2)
                valueLeft = round(self.state_values[(i, j),'L'],2)
                valueRight = round(self.state_values[(i, j),'R'],2)
                
                maxValue = max([valueUp,valueDown,valueLeft,valueRight])
                
                if (showAll == True):
                    if valueUp == maxValue:
                        out += '{:10s}'.format(str.format('*U({})',valueUp))
                    else:
                        out += '{:10s}'.format(str.format('U({})',valueUp))

                    if valueDown == maxValue:
                        out += '{:10s}'.format(str.format('*D({})',valueDown))
                    else:
                        out += '{:10s}'.format(str.format('D({})',valueDown))

                    if valueLeft == maxValue:
                        out += '{:10s}'.format(str.format('*L({})',valueLeft))
                    else:
                        out += '{:10s}'.format(str.format('L({})',valueLeft))

                    if valueRight == maxValue:
                        out += '{:10s}'.format(str.format('*R({})',valueRight))
                    else:
                        out += '{:10s}'.format(str.format('R({})',valueRight))
                else:
                    if valueUp == maxValue:
                        out += '{:10s}'.format(str.format('*U({})',valueUp))
                    elif valueDown == maxValue:
                        out += '{:10s}'.format(str.format('*D({})',valueDown))
                    elif valueLeft == maxValue:
                        out += '{:10s}'.format(str.format('*L({})',valueLeft))
                    elif valueRight == maxValue:
                        out += '{:10s}'.format(str.format('*R({})',valueRight))


                out += ' | ' #str().ljust(2)
            print(out)
        if (showAll):
            print('------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------')
        else:
            print('------------------------------------------------------------------')


if __name__ == "__main__":
    ag = Agent()
    ag.run(10000)
    print(ag.showValues(True))
    print(ag.showValues(False))
        
    plt.plot(ag.rewardByEpisodes.keys(), ag.rewardByEpisodes.values())
    plt.show()
    
    plt.hist(ag.rewardByEpisodes.values(), bins=list(range(-30,10)),label='Histogram of Number of Epochs vs accumulative Rewards')
    plt.show()
    
    plt.scatter(ag.rewardByEpisodes.keys(), ag.rewardByEpisodes.values())
    plt.show()



#SAMPLE OUTPUT
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# | U(-1.39)  D(-5.0)   L(-1.39)  *R(-0.43)  | U(-0.43)  *D(0.63)  L(-1.39)  *R(0.63)   | U(0.63)   *D(1.81)  L(-0.47)  R(0.29)    | U(-1.11)  D(-3.75)  L(-2.69)  *R(2.57)   | U(-2.1)   *D(4.49)  L(-2.5)   R(-2.38)   |
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# | *U(0)     *D(0)     *L(0)     *R(0)      | U(-0.43)  *D(1.81)  L(-5.0)   *R(1.81)   | U(0.63)   *D(3.12)  L(0.63)   R(-5.0)    | *U(0)     *D(0)     *L(0)     *R(0)      | U(-1.86)  *D(6.2)   L(-4.38)  R(1.58)    |
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# | U(-4.69)  D(-2.84)  L(0.58)   *R(1.81)   | U(0.63)   D(-5.0)   L(0.63)   *R(3.12)   | U(1.81)   *D(4.58)  L(1.81)   *R(4.58)   | U(-5.0)   D(6.17)   L(3.12)   *R(6.2)    | U(4.58)   *D(8.0)   L(4.58)   R(6.2)     |
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# | *U(-0.9)  D(-2.95)  L(-2.65)  R(-2.5)    | *U(0)     *D(0)     *L(0)     *R(0)      | U(3.12)   D(-5.0)   L(-5.0)   *R(6.2)    | U(4.58)   *D(8.0)   L(4.58)   *R(8.0)    | U(6.2)    *D(10.0)  L(6.2)    R(8.0)     |
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# | U(-2.8)   *D(-2.65) L(-2.93)  R(-2.66)   | U(-3.75)  D(-2.65)  L(-2.54)  *R(-2.5)   | *U(0)     *D(0)     *L(0)     *R(0)      | U(6.2)    D(8.0)    L(-5.0)   *R(10.0)   | *U(0)     *D(0)     *L(0)     *R(0)      |
# ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# None
# ------------------------------------------------------------------
# | *R(-0.43)  | *D(0.63)   | *D(1.81)   | *R(2.57)   | *D(4.49)   |
# ------------------------------------------------------------------
# | *U(0)      | *D(1.81)   | *D(3.12)   | *U(0)      | *D(6.2)    |
# ------------------------------------------------------------------
# | *R(1.81)   | *R(3.12)   | *D(4.58)   | *R(6.2)    | *D(8.0)    |
# ------------------------------------------------------------------
# | *U(-0.9)   | *U(0)      | *R(6.2)    | *D(8.0)    | *D(10.0)   |
# ------------------------------------------------------------------
# | *D(-2.65)  | *R(-2.5)   | *U(0)      | *R(10.0)   | *U(0)      |
# ------------------------------------------------------------------
